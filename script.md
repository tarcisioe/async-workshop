Async Workshop
==============

- Present first example (progress bars).

- Present threaded example (progress bars).
    - Show race for stdout.
    - Mention first limitation:
        - No control on context switching.
    - Solve with lock.
    - Mention limitations:
        - Threads are fairly resource intensive.
        - Threads arent' really parallel anyway.

- Take threads out of the table.

- First scheduler (callback, untimed).
    - We want concurrency: both functions need to make progress and we must
      gain responsivity.
    - First issue: loops.
        - We can't really loop. Then how do we... loop?
    - Implement functions recursively, but...
    - We can't really use recursion as well.
    - But we can emulate recursion by feeding functions to a scheduler.
        - Implement simple scheduler (append to ready queue, pop and run).
        - Point out timing issue (increase time for one of the functions).
        - This is the issue of __blocking__. We can't block on async.
    - Implement timed scheduler by using a sorted sleeping list.
        - Better timing!
        - We can do better with a priority queue.
        - Corner case: same deadline.
            - Fix it with a sequence number.

- Introduce the producer/consumer problem.
    - Show that it needs concurrency (otherwise it is mostly pointless).
    - The `queue` module helps us with the threaded version.
    - Implement a queue based on callbacks.
        - `get` must block. But we can't really block.
        - We must then save getters for later.
        - Therefore, `put` must "wake" a task.
    - We still cannot loop, so we go (pseudo-)recursive in the producer and the
      consumer.

- Try to introduce a queue close method.
    - We need a way to communicate the `get` caller that the queue is closed.
    - We introduce a result object which "communicates" the exception to the
      stack of the caller.
    - If the queue is closed, an exception is delivered to the caller.
    - The close method needs to clean up the waiting queue.

- So, the point of using callbacks is having small bits of code that can be
  alternated, so that, since no threading is being used, many tasks can be
  executed in a way that gives the illusion of simultaneity. Also, when tasks
  are effectively waiting for something (a sleep, a network event, user input,
  a file read...) we can actually do other things in the meantime.

- We are basically done with callbacks here. So let's introduce the idea of
  generators.

- Crash course on generators:
    - The iterator protocol.
    - The iterable protocol.
    - How generators implement the iterator/iterable protocol.
    - They therefore form __interruptible functions__, which we can use in a
      manner very similar to how we used callbacks.

- So now we will write a generator-based scheduler.
    - We go back to the first example (progress bars). We add `yield`s on each
      iteration of the loop.
    - We write the scheduler. The API will be mostly the same:
          - The `run` method will drive the generators we have until it has nothing
            more to drive.
          - The `call_soon` method will be renamed `new_task`, just so it matches the
            generator context a little better.

- The `yield from` syntax: delegating to another iterable (in our case, another
    generator).
    - Use the `yield from` syntax to delegate to a simple, empty generator.
      Looks and feels a bit stupid, but let's dive a little deeper.
    - We create a sleep method in the scheduler, very similar to the old
      `call_later`. This method will have a yield in the end.
    - This `sleep` method will "steal" the current running coroutine, so we
      need to keep track of which is the "current" coroutine.
    - We need to adapt the run loop. The way it will work will be much the same
      as the old scheduler.
    - We can now `yield from sched.sleep()` to sleep. Our code now looks fairly
      nice.

- The `async`/`await` syntax (coroutines):
    - Mostly it's syntax sugar. `await` stands for `yield from`, and `async`
      makes the function into a `coroutine`, which is, much like a generator,
      __an interruptible function__.
    - To drive the coroutines we don't use `next`, but a `.send` method.
    - And that's mostly it!

- So let's go back to the producer-consumer problem.
    - First of all, let's make it async and use sched.sleep.
    - Then let's build an `AsyncQueue`... again.
        - But it's much easier this time around: our `get` just steals the current
          task from the scheduler, while our put puts a task back there. Simple.
        - Closing the queue is as simple as checking if it is closed in get when
          there are no items.

- So we have seen two asynchronous programming models: coroutines and callbacks.
    But can they work together? Actually, yes.
    - Indeed `asyncio` follows this model, so, in understanding the standard
      async library, this is a great exercise.
    - Let's build our last scheduler now. We will get the callback-based
      scheduler and make it support coroutines as well.
    - Let's base our implementation on the callback-based scheduler. We first
      introduce a `current` member to the scheduler.
    - We then introduce a `Task` class which has a `__call__` method and wraps
      a coroutine. The body of `__call__` is mostly the code we had in the end
      of the pure-coroutine scheduler's `run` method.
    - The `new_task` method just delegates to `call_soon` and wraps the
      coroutine inside a `Task`.
    - The `sleep` method will delegate to `call_later`, scheduling the current
      callback to be called again later, and then calling `switch`.
    - Since `release_current` now releases a callback instead of a coroutine,
      the queue needs to use `call_soon` instead of `new_task`.
    - And that's it! We have a working hybrid scheduler.
    - It should run our old callback versions as well!

- Now for the final part of our workshop, let's talk a bit about I/O! Surely
    it is important, since every async library name ends with `io`!
    - I/O operations aren't much different than sleeps: they can block for any
      amount of time. The big differences are:
        - After blocking, they do something (either write or read data).
        - We don't really know how long they will block.
    - Therefore, the principle isn't much different: we need somewhere to leave
      waiting tasks while they wait, and a way to know when to wake them up.
    - Our first function will be `read_wait`, which will register a callback to
      run when a file is readable.
    - Our second function will be `write_wait`, which will pretty much do the
      same, but for writing.
    - We then need some way to find out if those files are readable or
      writeable and schedule the callback to run.
    - Enter the `select` function from the `select` module. This function takes
      a list of file descriptors and tells us what can be read and what can be
      written.
    - Now for the biggest change: we need to "merge" how we do sleeping and io:
        - Instead of popping from the sleeping queue, we need to peek into it.
        - Let's rename our "delta" to "timeout". This is now how long we are
          willing to wait for any writeable or readable file to become ready.
        - We limit the timeout to above or equal to 0 (we can't wait for a
          negative amount of time), and if there is nothing sleeping, we set
          it to `None`, meaning we can wait for as long as needed.
        - We then call `select` to find out which files are readable/writeable.
          The timeout on `select` is now working as our `sleep`.
        - Now whatever `select` returned can be added to the `ready` queue.
        - Then we check the sleeping queue for deadlines that have passed and
          schedule those in the `ready` queue.
        - We then provide some nicer `async` functions which are to `read_wait`
          and `write_wait` what `sleep` is to `call_later`.
    - Let's then provide a `AsyncSocket` as a wrapper around a socket and a
      scheduler, and a `make_async_socket` function in the scheduler.

- We can then use this scheduler to make a simple TCP echo server.
- To end our demonstration, we can run the server along with any other example,
  like the progress bars.

- And that's it!
