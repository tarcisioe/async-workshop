from collections import deque


class Scheduler:
    def __init__(self):
        self.ready = deque()

    def call_soon(self, task):
        self.ready.append(task)

    def run(self):
        while self.ready:
            task = self.ready.popleft()
            task()
