from collections import deque

from .scheduler import Scheduler

sched = Scheduler()


class QueueClosed(Exception):
    pass


class Result:
    def __init__(self, value=None, exc=None):
        self.value = value
        self.exc = exc

    def get(self):
        if self.exc:
            raise self.exc

        return self.value


class AsyncQueue:
    def __init__(self):
        self.items = deque()
        self.waiting = deque()
        self._closed = False

    def put(self, item):
        if self._closed:
            raise QueueClosed()

        self.items.append(item)

        if self.waiting:
            func = self.waiting.popleft()
            # Let's avoid calling this directly, so we don't have long recursion
            # chains.
            sched.call_soon(func)

    def get(self, callback):
        # What do we do if the queue is closed?

        if self.items:
            # This is the easy case. There is no issue if it is closed, though.
            sched.call_soon(lambda: callback(Result(value=self.items.popleft())))
        else:
            # Maybe the queue is closed, so we know there won't be new items.
            # What do we do?
            if self._closed:
                sched.call_soon(lambda: callback(Result(exc=QueueClosed())))

            # Now what? Well, guess we need to keep this guy for later.
            self.waiting.append(lambda: self.get(callback))

    def close(self):
        self._closed = True

        while self.waiting:
            func = self.waiting.popleft()
            sched.call_soon(func)


def producer(items, count):
    def _run(i):
        if i >= count:
            print('Producer done')
            items.close()
            return

        print('Producing', i)
        items.put(i)
        sched.call_later(1, lambda: _run(i + 1))

    _run(0)


def consumer(items):
    def _consume(result):
        try:
            item = result.get()
        except QueueClosed:
            print('Consumer done')
            return

        print('Consuming', item)
        sched.call_soon(lambda: consumer(items))

    items.get(callback=_consume)


q = AsyncQueue()
sched.call_soon(lambda: producer(q, 10))
sched.call_soon(lambda: consumer(q))
sched.run()
