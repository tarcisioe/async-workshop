import time
from collections import deque

from ...util import PriorityQueue


class Scheduler:
    def __init__(self):
        self.ready = deque()
        self.sleeping = PriorityQueue()
        self.sequence = 0  # Fix for corner case.

    def call_soon(self, task):
        self.ready.append(task)

    def call_later(self, delay, func):
        deadline = time.time() + delay
        # self.sleeping.append((deadline, func))  # bad
        # self.sleeping.sort()
        self.sleeping.push((deadline, self.sequence, func))
        self.sequence += 1

    def run(self):
        while self.ready or self.sleeping:
            if not self.ready:
                # deadline, func = self.sleeping.pop(0)  # slow
                deadline, _, func = self.sleeping.pop()
                delta = deadline - time.time()
                if delta > 0:
                    time.sleep(delta)
                self.ready.append(func)

            while self.ready:
                task = self.ready.popleft()
                task()
