from proggy import BarInfo
from proggy.tty import TTYMultiProgressBar

from .scheduler import Scheduler

sched = Scheduler()


def task_1(bar):
    if bar.progress >= bar.total:
        return
    bar.progress += 1
    sched.call_later(0.1, lambda: task_1(bar))


def task_2(bar):
    if bar.progress <= 0:
        return
    bar.progress -= 1
    sched.call_later(0.1, lambda: task_2(bar))


with TTYMultiProgressBar(
    bar_infos=[
        BarInfo(size=30, total=40),
        BarInfo(size=30, total=40, progress=40, characters=' -=#'),
    ],
) as mp:
    mp.draw()
    sched.call_soon(lambda: task_1(mp.bar_at(0)))
    sched.call_soon(lambda: task_2(mp.bar_at(1)))
    sched.run()
