from collections import deque

from .scheduler import Scheduler, switch

sched = Scheduler()


class QueueClosed(Exception):
    pass


class AsyncQueue:
    def __init__(self):
        self.items = deque()
        self.waiting = deque()
        self._closed = False

    def close(self):
        self._closed = True

        if self.waiting and not self.items:
            sched.call_soon(self.waiting.popleft())

    def put(self, item):
        if self._closed:
            raise QueueClosed()

        self.items.append(item)

        if self.waiting:
            sched.call_soon(self.waiting.popleft())

    async def get(self):
        # If there was no close method, this could be an if.
        # Since there is, there is a chance a task could be
        # awoken by `close()`.
        while not self.items:
            if self._closed:
                raise QueueClosed()
            self.waiting.append(sched.release_current())
            await switch()

        return self.items.popleft()


async def producer(items, count):
    for i in range(count):
        print('Producing', i)
        items.put(i)
        await sched.sleep(1)

    items.close()
    print('Producer done')


async def consumer(items):
    while True:
        try:
            item = await items.get()
        except QueueClosed:
            break
        print('Consuming', item)
    print('Consumer done')


q = AsyncQueue()
sched.new_task(producer(q, 10))
sched.new_task(consumer(q))
sched.run()
