import time
from collections import deque

from ...util import PriorityQueue


class EmptyAwaitable:
    def __await__(self):
        yield


def switch():
    return EmptyAwaitable()


class Task:
    def __init__(self, sched, coro):
        self.sched = sched
        self.coro = coro

    def __call__(self):
        self.sched.current = self

        try:
            self.coro.send(None)
        except StopIteration:
            pass
        else:
            if self.sched.current is not None:
                self.sched.call_soon(self)


class Scheduler:
    def __init__(self):
        self.ready = deque()
        self.sleeping = PriorityQueue()
        self.sequence = 0
        self.current = None

    def call_soon(self, task):
        self.ready.append(task)

    def call_later(self, delay, func):
        deadline = time.time() + delay
        self.sleeping.push((deadline, self.sequence, func))
        self.sequence += 1

    def run(self):
        while self.ready or self.sleeping:
            if not self.ready:
                deadline, _, func = self.sleeping.pop()
                delta = deadline - time.time()
                if delta > 0:
                    time.sleep(delta)
                self.ready.append(func)

            while self.ready:
                task = self.ready.popleft()
                task()

    def new_task(self, coro):
        self.call_soon(Task(self, coro))

    def release_current(self):
        callback = self.current
        self.current = None
        return callback

    async def sleep(self, delay):
        self.call_later(delay, self.release_current())
        await switch()
