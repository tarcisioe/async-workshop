# The iterable protocol:
class Countup:
    def __init__(self, limit):
        self.limit = limit

    def __iter__(self):
        return CountupIterator(self)


# What is returned by `__iter__` must be an Iterator (have a `__next__`).
class CountupIterator:
    def __init__(self, iterable):
        self.current = 0
        self.iterable = iterable

    def __next__(self):
        if self.current >= self.iterable.limit:
            raise StopIteration()

        value = self.current
        self.current += 1
        return value


for i in Countup(10):
    print(i)

# As an example, lists are iterables:
a = [1, 2, 3, 4]
a_iter = iter(a)
while True:
    try:
        print(next(a_iter))
    except StopIteration:
        break


# Generators are python functions that define an iterable:
def generate_a_b_c():
    yield 'a'
    yield 'b'
    yield 'c'


# They kind of do nothing at all by themselves:
print(generate_a_b_c())

# They must be driven by something (like a `for` loop):
for c in generate_a_b_c():
    print(c)

# So every time you call `next` on a generator, it executes some code and
# stops, and you can resume it later by calling `next` again...

# Remember the callbacks? Our goal with them was having small code blocks to
# run alternating. Generators give us something very similar. Indeed, similar
# enough that we can use them to the same effect.
