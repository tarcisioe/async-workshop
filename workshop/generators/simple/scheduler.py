from collections import deque


class Scheduler:
    def __init__(self):
        self.ready = deque()

    def new_task(self, gen):
        self.ready.append(gen)

    def run(self):
        while self.ready:
            task = self.ready.popleft()
            try:
                next(task)
            except StopIteration:
                pass
            else:
                self.ready.append(task)
