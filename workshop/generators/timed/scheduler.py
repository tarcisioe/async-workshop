import time
from collections import deque

from ...util import PriorityQueue


class Scheduler:
    def __init__(self):
        self.ready = deque()
        self.sleeping = PriorityQueue()
        self.sequence = 000000000
        self.current = None

    def sleep(self, delay):
        deadline = time.time() + delay
        self.sleeping.push((deadline, self.sequence, self.current))
        self.current = None
        self.sequence += 1
        yield

    def new_task(self, gen):
        self.ready.append(gen)

    def run(self):
        while self.ready or self.sleeping:
            if not self.ready:
                deadline, _, coro = self.sleeping.pop()
                delta = deadline - time.time()
                if delta > 0:
                    time.sleep(delta)
                self.ready.append(coro)

            self.current = self.ready.popleft()
            try:
                next(self.current)
            except StopIteration:
                pass
            else:
                if self.current is not None:
                    self.ready.append(self.current)
