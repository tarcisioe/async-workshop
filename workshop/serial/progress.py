import time

from proggy import BarInfo
from proggy.tty import TTYMultiProgressBar


def task_1(bar):
    while bar.progress < bar.total:
        bar.progress += 1
        time.sleep(0.1)


def task_2(bar):
    while bar.progress > 0:
        bar.progress -= 1
        time.sleep(0.1)


with TTYMultiProgressBar(
    bar_infos=[
        BarInfo(size=30, total=40),
        BarInfo(size=30, total=40, progress=40, characters=' -=#'),
    ],
) as mp:
    mp.draw()
    task_1(mp.bar_at(0))
    task_2(mp.bar_at(1))
