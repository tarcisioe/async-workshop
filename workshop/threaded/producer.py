"""A real, reasonable producer-consumer example."""
import queue
import time
from threading import Thread


def producer(items, count):
    for i in range(count):
        print('Producing', i)
        items.put(i)
        time.sleep(1)

    items.put(None)
    print('Producer done')


def consumer(items):
    while True:
        item = items.get()
        if item is None:
            break
        print('Consuming', item)
    print('Consumer done')


q = queue.Queue()
Thread(target=producer, args=(q, 10)).start()
Thread(target=consumer, args=(q,)).start()
