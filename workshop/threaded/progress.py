import time
from threading import Lock, Thread

from proggy import BarInfo
from proggy.tty import TTYMultiProgressBar

# Introduce later. This is needed because drawing is not atomic, and
# the stdout (tty) is a resource.
stdout_lock = Lock()


def task_1(bar):
    while bar.progress < bar.total:
        with stdout_lock:
            bar.progress += 1
        time.sleep(0.1)


def task_2(bar):
    while bar.progress > 0:
        with stdout_lock:
            bar.progress -= 1
        time.sleep(0.1)


with TTYMultiProgressBar(
    bar_infos=[
        BarInfo(size=30, total=40),
        BarInfo(size=30, total=40, progress=40, characters=' -=#'),
    ],
) as mp:
    mp.draw()
    a = Thread(target=task_1, args=(mp.bar_at(0),))
    b = Thread(target=task_2, args=(mp.bar_at(1),))
    a.start()
    b.start()
    a.join()
    b.join()
