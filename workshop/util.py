from heapq import heappop, heappush


class PriorityQueue:
    def __init__(self):
        self._queue = []

    def __len__(self):
        return len(self._queue)

    def push(self, value):
        heappush(self._queue, value)

    def pop(self):
        return heappop(self._queue)

    def peek(self):
        return self._queue[0]
